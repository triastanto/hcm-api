<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrgTextTable extends Migration
{
    public $tableName;

    public function __construct()
    {
        $this->tableName = config('hcm.table.org_text');
    }

    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->string('PV', 2)->nullable()->default(null);
            $table->string('OT', 2)->nullable()->default(null);
            $table->string('ObjectID', 8)->nullable()->default(null);
            $table->string('Objectname', 40)->nullable()->default(null);
            $table->string('Objectabbr', 12)->nullable()->default(null);
            $table->date('Startdate')->nullable()->default(null);
            $table->date('EndDate')->nullable()->default(null);
            $table->string('IT', 4)->nullable()->default(null);
            $table->date('LSTUPDT')->nullable()->default(null);
        });
    }

     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}
