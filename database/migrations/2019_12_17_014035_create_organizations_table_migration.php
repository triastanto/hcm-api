<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

class CreateOrganizationsTableMigration extends Migration
{
    public function up()
    {
        Schema::create('organizations', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('sap_object_id', 10);
            $table->string('sap_object_abbr', 10);

            // closure related columns
            $table->integer('parent_id')->unsigned()->nullable();
            $table->integer('closure_position', false, true);

            $table->timestamps();

            $table->foreign('parent_id')
                ->references('id')
                ->on('organizations')
                ->onDelete('set null');
        });

        Schema::create('organization_closure', function (Blueprint $table) {
            $table->increments('closure_id');

            $table->integer('ancestor', false, true);
            $table->integer('descendant', false, true);
            $table->integer('depth', false, true);

            $table->foreign('ancestor')
                ->references('id')
                ->on('organizations')
                ->onDelete('cascade');

            $table->foreign('descendant')
                ->references('id')
                ->on('organizations')
                ->onDelete('cascade');
        });
    }

    public function down()
    {
        Schema::table('organization_closure', function (Blueprint $table) {
            Schema::dropIfExists('organization_closure');
        });

        Schema::table('organizations', function (Blueprint $table) {
            Schema::dropIfExists('organizations');
        });
    }
}
