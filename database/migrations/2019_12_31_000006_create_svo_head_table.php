<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSvoHeadTable extends Migration
{
    public $tableName;

    public function __construct()
    {
        $this->tableName = config('hcm.table.svo_head');
    }

    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->string('OT', 2);
            $table->string('ObjectID', 10);
            $table->string('PV', 2);
            $table->string('Rel', 3);
            $table->date('StartDate');
            $table->date('EndDate');
            $table->string('RO', 2);
            $table->string('IDrelatedobject', 8);
        });
    }

     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}
