<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

class CreateOrghistoriesTableMigration extends Migration
{
    public function up()
    {
        Schema::create('orghistories', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('sap_object_id', 10);
            $table->string('sap_object_abbr', 10);
            $table->unsignedInteger('organization_id');

            // closure related columns
            $table->integer('parent_id')->unsigned()->nullable();
            $table->integer('closure_position', false, true);

            $table->timestamps();

            $table->foreign('parent_id')
                ->references('id')
                ->on('orghistories')
                ->onDelete('set null');
        });

        Schema::create('orghistory_closure', function (Blueprint $table) {
            $table->increments('closure_id');

            $table->integer('ancestor', false, true);
            $table->integer('descendant', false, true);
            $table->integer('depth', false, true);

            $table->foreign('ancestor')
                ->references('id')
                ->on('orghistories')
                ->onDelete('cascade');

            $table->foreign('descendant')
                ->references('id')
                ->on('orghistories')
                ->onDelete('cascade');
        });
    }

    public function down()
    {
        Schema::table('orghistory_closure', function (Blueprint $table) {
            Schema::dropIfExists('orghistory_closure');
        });

        Schema::table('orghistories', function (Blueprint $table) {
            Schema::dropIfExists('orghistories');
        });
    }
}
