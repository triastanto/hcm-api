<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSPositionsTable extends Migration
{
    public $tableName;

    public function __construct()
    {
        $this->tableName = config('hcm.table.s_positions');
    }

    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->string('PV', 2);
            $table->string('OT', 2);
            $table->string('ObjectID', 10);
            $table->string('Objectname', 100);
            $table->string('Objectabbr', 13);
            $table->date('StartDate');
            $table->date('EndDate');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}
