<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrgunitTable extends Migration
{
    public $tableName;

    public function __construct()
    {
        $this->tableName = config('hcm.table.orgunit');
    }

    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->string('OT', 2)->nullable()->default(null);
            $table->string('ObjectID', 8)->nullable()->default(null);
            $table->string('S', 1)->nullable()->default(null);
            $table->string('Rel', 3)->nullable()->default(null);
            $table->date('Startdate')->nullable()->default(null);
            $table->date('EndDate')->nullable()->default(null);
            $table->string('RO', 2)->nullable()->default(null);
            $table->string('IDrelatedobject', 8)->nullable()->default(null);
            $table->string('IDObjAbbr', 10)->nullable()->default(null);
            $table->string('ObjAbbr', 100)->nullable()->default(null);
            $table->date('LSTUPDT')->nullable()->default(null);
        });
    }

     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}
