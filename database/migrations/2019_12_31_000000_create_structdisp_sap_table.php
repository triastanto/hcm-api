<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStructdispSapTable extends Migration
{
    public $tableName;

    public function __construct()
    {
        $this->tableName = config('hcm.table.structdisp_sap');
    }

    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->integer('no')->nullable()->default(null)->comment('NO URUT RELASI');
            $table->string('empnik', 8)->nullable()->default(null)->comment('EMPLOYEE NIK');
            $table->string('empname', 40)->nullable()->default(null)->comment('EMPLOYEE NAME');
            $table->string('empposid', 8)->nullable()->default(null)->comment(' Posisi-ID atasan (untuk urut 1 = posisi-ID employee)');
            $table->string('emp_hrp1000_s_short', 20)->nullable()->default(null)->comment('Abbreviasi Posisi');
            $table->string('emppostx', 40)->nullable()->default(null)->comment('Nama Posisi (untuk urut 1 = nama posisi employee)');
            $table->string('emporid', 8)->nullable()->default(null)->comment('Org.ID atasan (untuk urut 1 = Org.ID employee)');
            $table->string('emportx', 40)->nullable()->default(null)->comment('Nama Org. (untuk urut 1 = nama org. employee)');
            $table->string('emp_hrp1000_o_short', 12)->nullable()->default(null)->comment('Abbreviasi Org.');
            $table->string('empjobid', 8)->nullable()->default(null)->comment('Job-ID');
            $table->string('empjobstext', 40)->nullable()->default(null)->comment('Job Name');
            $table->string('emppersk', 2)->nullable()->default(null)->comment('employee subgroup posisi');
            $table->string('emp_t503t_ptext', 20)->nullable()->default(null)->comment('employee subgroup text posisi');
            $table->string('empkostl', 10)->nullable()->default(null)->comment(' cost center');
            $table->string('emp_cskt_ltext', 40)->nullable()->default(null)->comment('nama cost center');
            $table->string('dirnik', 8)->nullable()->default(null)->comment('NIK Atasan (untuk urut 1 = nik Employee)');
            $table->string('dirname', 40)->nullable()->default(null)->comment('Nama Atasan');
            $table->date('LSTUPDT')->nullable()->default(null)->comment('last update');
        });
    }

     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}
