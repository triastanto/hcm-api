<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class OrgHCITablesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //path to sql file
        $org_text = base_path('database/seeds/dummy_org_text-20191231.sql');
        $orgunit = base_path('database/seeds/dummy_orgunit-20191231.sql');
        $svo_head = base_path('database/seeds/dummy_svo_head-20191230.sql');
        $s_positions = base_path('database/seeds/dummy_s_positions-20191230.sql');
        $structdisp_sap = base_path('database/seeds/dummy_structdisp_sap-20191231.sql');
        
        //collect contents and pass to DB::unprepared
        DB::unprepared(file_get_contents($org_text));
        DB::unprepared(file_get_contents($orgunit));
        DB::unprepared(file_get_contents($svo_head));
        DB::unprepared(file_get_contents($s_positions));
        DB::unprepared(file_get_contents($structdisp_sap));

    }
}
