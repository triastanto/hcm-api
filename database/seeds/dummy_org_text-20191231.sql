INSERT INTO dummy_org_text
(PV, OT, ObjectID, Objectname, Objectabbr, Startdate, EndDate, IT, LSTUPDT)
VALUES('01', 'O', '50003083', 'Divisi Pabrik Pengerolan BLP', '34200', '1900-01-01', '2011-12-08', NULL, '2019-12-31');
INSERT INTO dummy_org_text
(PV, OT, ObjectID, Objectname, Objectabbr, Startdate, EndDate, IT, LSTUPDT)
VALUES('01', 'O', '50003083', 'Divisi Pabrik Pengerolan BLP', '34200', '2011-12-09', '2012-01-30', NULL, '2019-12-31');
INSERT INTO dummy_org_text
(PV, OT, ObjectID, Objectname, Objectabbr, Startdate, EndDate, IT, LSTUPDT)
VALUES('01', 'O', '50003083', 'Divisi Hot Strip Mill Plant', '34200', '2012-01-31', '2012-09-03', NULL, '2019-12-31');
INSERT INTO dummy_org_text
(PV, OT, ObjectID, Objectname, Objectabbr, Startdate, EndDate, IT, LSTUPDT)
VALUES('01', 'O', '50003083', 'Divisi Hot Strip Mill', '34200', '2012-09-04', '9999-12-31', NULL, '2019-12-31');
INSERT INTO dummy_org_text
(PV, OT, ObjectID, Objectname, Objectabbr, Startdate, EndDate, IT, LSTUPDT)
VALUES('01', 'O', '50003102', 'Dinas Operasi Pengerolan BLP', '34202', '1900-01-01', '2010-06-29', NULL, '2019-12-31');
INSERT INTO dummy_org_text
(PV, OT, ObjectID, Objectname, Objectabbr, Startdate, EndDate, IT, LSTUPDT)
VALUES('01', 'O', '50003102', 'Dinas Operasi Pengerolan BLP', '34203', '2010-06-30', '2011-12-08', NULL, '2019-12-31');
INSERT INTO dummy_org_text
(PV, OT, ObjectID, Objectname, Objectabbr, Startdate, EndDate, IT, LSTUPDT)
VALUES('01', 'O', '50003102', 'Dinas Operasi Pengerolan BLP', '34203', '2011-12-09', '2012-01-30', NULL, '2019-12-31');
INSERT INTO dummy_org_text
(PV, OT, ObjectID, Objectname, Objectabbr, Startdate, EndDate, IT, LSTUPDT)
VALUES('01', 'O', '50003102', 'Dinas Rolling Operation HSM', '34203', '2012-01-31', '2014-04-14', NULL, '2019-12-31');
INSERT INTO dummy_org_text
(PV, OT, ObjectID, Objectname, Objectabbr, Startdate, EndDate, IT, LSTUPDT)
VALUES('01', 'O', '50003102', 'Dinas Rolling Operation HSM', '34202', '2014-04-15', '9999-12-31', NULL, '2019-12-31');
INSERT INTO dummy_org_text
(PV, OT, ObjectID, Objectname, Objectabbr, Startdate, EndDate, IT, LSTUPDT)
VALUES('01', 'O', '50003103', 'Dinas Operasi Penanganan Akhir Material', '34203', '1900-01-01', '2010-06-29', NULL, '2019-12-31');
INSERT INTO dummy_org_text
(PV, OT, ObjectID, Objectname, Objectabbr, Startdate, EndDate, IT, LSTUPDT)
VALUES('01', 'O', '50003103', 'Dinas Operasi Penanganan Akhir Material', '34205', '2010-06-30', '2011-12-08', NULL, '2019-12-31');
INSERT INTO dummy_org_text
(PV, OT, ObjectID, Objectname, Objectabbr, Startdate, EndDate, IT, LSTUPDT)
VALUES('01', 'O', '50003103', 'Dinas Operasi Penanganan Akhir Material', '34205', '2011-12-09', '2012-01-30', NULL, '2019-12-31');
INSERT INTO dummy_org_text
(PV, OT, ObjectID, Objectname, Objectabbr, Startdate, EndDate, IT, LSTUPDT)
VALUES('01', 'O', '50003103', 'Dinas Shearing Line & WIP', '34204', '2012-01-31', '9999-12-31', NULL, '2019-12-31');
INSERT INTO dummy_org_text
(PV, OT, ObjectID, Objectname, Objectabbr, Startdate, EndDate, IT, LSTUPDT)
VALUES('01', 'O', '50003507', 'Seksi Operasi Pengerolan', '34202', '1900-01-01', '2009-04-16', NULL, '2019-12-31');
INSERT INTO dummy_org_text
(PV, OT, ObjectID, Objectname, Objectabbr, Startdate, EndDate, IT, LSTUPDT)
VALUES('01', 'O', '50003507', 'Seksi Operasi Pengerolan BLP', '34202', '2009-04-17', '2010-06-29', NULL, '2019-12-31');
INSERT INTO dummy_org_text
(PV, OT, ObjectID, Objectname, Objectabbr, Startdate, EndDate, IT, LSTUPDT)
VALUES('01', 'O', '50003507', 'Seksi Operasi Pengerolan BLP', '34203', '2010-06-30', '2014-07-01', NULL, '2019-12-31');
INSERT INTO dummy_org_text
(PV, OT, ObjectID, Objectname, Objectabbr, Startdate, EndDate, IT, LSTUPDT)
VALUES('01', 'O', '50003508', 'Seksi Roll & Grinding Machine', '34202', '1900-01-01', '2009-04-18', NULL, '2019-12-31');
INSERT INTO dummy_org_text
(PV, OT, ObjectID, Objectname, Objectabbr, Startdate, EndDate, IT, LSTUPDT)
VALUES('01', 'O', '50003552', 'Urusan Operasi Pengerolan Awal', '34202', '1900-01-01', '2010-05-31', NULL, '2019-12-31');
INSERT INTO dummy_org_text
(PV, OT, ObjectID, Objectname, Objectabbr, Startdate, EndDate, IT, LSTUPDT)
VALUES('01', 'O', '50003552', 'Urusan Operasi Pengerolan Awal', '34203', '2010-06-01', '2014-07-01', NULL, '2019-12-31');
INSERT INTO dummy_org_text
(PV, OT, ObjectID, Objectname, Objectabbr, Startdate, EndDate, IT, LSTUPDT)
VALUES('01', 'O', '50003553', 'Urusan Operasi Pengerolan Akhir', '34202', '1900-01-01', '2010-05-31', NULL, '2019-12-31');
INSERT INTO dummy_org_text
(PV, OT, ObjectID, Objectname, Objectabbr, Startdate, EndDate, IT, LSTUPDT)
VALUES('01', 'O', '50003553', 'Urusan Operasi Pengerolan Akhir', '34203', '2010-06-01', '2014-07-01', NULL, '2019-12-31');
INSERT INTO dummy_org_text
(PV, OT, ObjectID, Objectname, Objectabbr, Startdate, EndDate, IT, LSTUPDT)
VALUES('01', 'O', '50003644', 'Urusan Grinding & Assembly Chock WR', '34202', '2004-10-18', '2009-04-18', NULL, '2019-12-31');
INSERT INTO dummy_org_text
(PV, OT, ObjectID, Objectname, Objectabbr, Startdate, EndDate, IT, LSTUPDT)
VALUES('01', 'O', '50003645', 'Urusan Prwt Chock & Bearing', '34202', '2004-10-18', '2009-04-18', NULL, '2019-12-31');
INSERT INTO dummy_org_text
(PV, OT, ObjectID, Objectname, Objectabbr, Startdate, EndDate, IT, LSTUPDT)
VALUES('01', 'O', '50003720', 'Urusan Bengkel SL & HSPM', '34203', '1900-01-01', '2009-04-18', NULL, '2019-12-31');
INSERT INTO dummy_org_text
(PV, OT, ObjectID, Objectname, Objectabbr, Startdate, EndDate, IT, LSTUPDT)
VALUES('01', 'O', '50013794', 'Seksi Operasi Shearing Line', '34203', '1996-10-31', '2009-04-18', NULL, '2019-12-31');
INSERT INTO dummy_org_text
(PV, OT, ObjectID, Objectname, Objectabbr, Startdate, EndDate, IT, LSTUPDT)
VALUES('01', 'O', '50013810', 'Urusan Operasi HSPM', '34203', '1996-10-31', '2009-04-18', NULL, '2019-12-31');
INSERT INTO dummy_org_text
(PV, OT, ObjectID, Objectname, Objectabbr, Startdate, EndDate, IT, LSTUPDT)
VALUES('01', 'O', '50025260', 'Seksi Operasi HSPM & Bengkel SL', '34203', '1999-03-01', '2009-04-18', NULL, '2019-12-31');
INSERT INTO dummy_org_text
(PV, OT, ObjectID, Objectname, Objectabbr, Startdate, EndDate, IT, LSTUPDT)
VALUES('01', 'O', '50027025', 'Urusan Operasi Penggulungan', '34202', '2000-08-21', '2010-05-31', NULL, '2019-12-31');
INSERT INTO dummy_org_text
(PV, OT, ObjectID, Objectname, Objectabbr, Startdate, EndDate, IT, LSTUPDT)
VALUES('01', 'O', '50027025', 'Urusan Operasi Penggulungan', '34203', '2010-06-01', '2014-07-01', NULL, '2019-12-31');
INSERT INTO dummy_org_text
(PV, OT, ObjectID, Objectname, Objectabbr, Startdate, EndDate, IT, LSTUPDT)
VALUES('01', 'O', '50027038', 'Urusan Prwt Mesin Gerinda', '34202', '2000-08-21', '2009-04-18', NULL, '2019-12-31');
INSERT INTO dummy_org_text
(PV, OT, ObjectID, Objectname, Objectabbr, Startdate, EndDate, IT, LSTUPDT)
VALUES('01', 'O', '50033089', 'Dinas Strategi Pengerolan & Pemotongan', '34204', '2004-10-04', '2010-06-29', NULL, '2019-12-31');
INSERT INTO dummy_org_text
(PV, OT, ObjectID, Objectname, Objectabbr, Startdate, EndDate, IT, LSTUPDT)
VALUES('01', 'O', '50033089', 'Dinas Strategi Pengerolan & Pemotongan', '34202', '2010-06-30', '2011-12-08', NULL, '2019-12-31');
INSERT INTO dummy_org_text
(PV, OT, ObjectID, Objectname, Objectabbr, Startdate, EndDate, IT, LSTUPDT)
VALUES('01', 'O', '50033089', 'Dinas Strategi Pengerolan & Pemotongan', '34202', '2011-12-09', '2012-01-30', NULL, '2019-12-31');
INSERT INTO dummy_org_text
(PV, OT, ObjectID, Objectname, Objectabbr, Startdate, EndDate, IT, LSTUPDT)
VALUES('01', 'O', '50033089', 'Dinas Rolling & Cutting Strategy HSM', '34202', '2012-01-31', '2014-07-01', NULL, '2019-12-31');
INSERT INTO dummy_org_text
(PV, OT, ObjectID, Objectname, Objectabbr, Startdate, EndDate, IT, LSTUPDT)
VALUES('01', 'O', '50033090', 'Seksi Strategi Operasi Dapur', '34204', '2004-10-04', '2009-04-18', NULL, '2019-12-31');
INSERT INTO dummy_org_text
(PV, OT, ObjectID, Objectname, Objectabbr, Startdate, EndDate, IT, LSTUPDT)
VALUES('01', 'O', '50033091', 'Seksi Strategi Operasi Pengerolan', '34204', '2004-10-04', '2010-06-29', NULL, '2019-12-31');
INSERT INTO dummy_org_text
(PV, OT, ObjectID, Objectname, Objectabbr, Startdate, EndDate, IT, LSTUPDT)
VALUES('01', 'O', '50033091', 'Seksi Strategi Operasi Pengerolan', '34202', '2010-06-30', '2014-07-01', NULL, '2019-12-31');
INSERT INTO dummy_org_text
(PV, OT, ObjectID, Objectname, Objectabbr, Startdate, EndDate, IT, LSTUPDT)
VALUES('01', 'O', '50033092', 'Seksi Analisis Peningkatan Kualitas', '34204', '2004-10-04', '2010-06-29', NULL, '2019-12-31');
INSERT INTO dummy_org_text
(PV, OT, ObjectID, Objectname, Objectabbr, Startdate, EndDate, IT, LSTUPDT)
VALUES('01', 'O', '50033092', 'Seksi Analisis Peningkatan Kualitas', '34202', '2010-06-30', '2014-07-01', NULL, '2019-12-31');
INSERT INTO dummy_org_text
(PV, OT, ObjectID, Objectname, Objectabbr, Startdate, EndDate, IT, LSTUPDT)
VALUES('01', 'O', '50033093', 'Seksi Strategi Komputer Proses', '34204', '2004-10-04', '2010-06-29', NULL, '2019-12-31');
INSERT INTO dummy_org_text
(PV, OT, ObjectID, Objectname, Objectabbr, Startdate, EndDate, IT, LSTUPDT)
VALUES('01', 'O', '50033093', 'Seksi Strategi Komputer Proses', '34202', '2010-06-30', '2014-07-01', NULL, '2019-12-31');
INSERT INTO dummy_org_text
(PV, OT, ObjectID, Objectname, Objectabbr, Startdate, EndDate, IT, LSTUPDT)
VALUES('01', 'O', '50035234', 'Urusan Operasi Dapur', '34202', '2004-10-04', '2009-04-18', NULL, '2019-12-31');
INSERT INTO dummy_org_text
(PV, OT, ObjectID, Objectname, Objectabbr, Startdate, EndDate, IT, LSTUPDT)
VALUES('01', 'O', '50060005', 'Seksi Operasi Dapur', '34202', '2008-03-13', '2010-06-29', NULL, '2019-12-31');
INSERT INTO dummy_org_text
(PV, OT, ObjectID, Objectname, Objectabbr, Startdate, EndDate, IT, LSTUPDT)
VALUES('01', 'O', '50060005', 'Seksi Operasi Dapur', '34203', '2010-06-30', '2014-07-01', NULL, '2019-12-31');
INSERT INTO dummy_org_text
(PV, OT, ObjectID, Objectname, Objectabbr, Startdate, EndDate, IT, LSTUPDT)
VALUES('01', 'O', '50060026', 'Urusan Operasi Dapur', '34202', '2008-03-13', '2010-06-29', NULL, '2019-12-31');
INSERT INTO dummy_org_text
(PV, OT, ObjectID, Objectname, Objectabbr, Startdate, EndDate, IT, LSTUPDT)
VALUES('01', 'O', '50060026', 'Urusan Operasi Dapur', '34203', '2010-06-30', '2014-07-01', NULL, '2019-12-31');
INSERT INTO dummy_org_text
(PV, OT, ObjectID, Objectname, Objectabbr, Startdate, EndDate, IT, LSTUPDT)
VALUES('01', 'O', '50060053', 'Seksi Penggerindaan Roll', '34202', '2008-03-13', '2010-06-29', NULL, '2019-12-31');
INSERT INTO dummy_org_text
(PV, OT, ObjectID, Objectname, Objectabbr, Startdate, EndDate, IT, LSTUPDT)
VALUES('01', 'O', '50060053', 'Seksi Penggerindaan Roll', '34204', '2010-06-30', '2014-07-01', NULL, '2019-12-31');
INSERT INTO dummy_org_text
(PV, OT, ObjectID, Objectname, Objectabbr, Startdate, EndDate, IT, LSTUPDT)
VALUES('01', 'O', '50060055', 'Urusan Penggerindaan', '34202', '2008-03-13', '2010-06-29', NULL, '2019-12-31');
INSERT INTO dummy_org_text
(PV, OT, ObjectID, Objectname, Objectabbr, Startdate, EndDate, IT, LSTUPDT)
VALUES('01', 'O', '50060055', 'Urusan Penggerindaan', '34204', '2010-06-30', '2014-07-01', NULL, '2019-12-31');
INSERT INTO dummy_org_text
(PV, OT, ObjectID, Objectname, Objectabbr, Startdate, EndDate, IT, LSTUPDT)
VALUES('01', 'O', '50060056', 'Urusan Perawatan Mesin Gerinda', '34202', '2008-03-13', '2010-06-29', NULL, '2019-12-31');
INSERT INTO dummy_org_text
(PV, OT, ObjectID, Objectname, Objectabbr, Startdate, EndDate, IT, LSTUPDT)
VALUES('01', 'O', '50060056', 'Urusan Perawatan Mesin Gerinda', '34204', '2010-06-30', '2014-07-01', NULL, '2019-12-31');
INSERT INTO dummy_org_text
(PV, OT, ObjectID, Objectname, Objectabbr, Startdate, EndDate, IT, LSTUPDT)
VALUES('01', 'O', '50060088', 'Seksi Prwt Chock & Bantalan', '34202', '2008-03-13', '2010-06-29', NULL, '2019-12-31');
INSERT INTO dummy_org_text
(PV, OT, ObjectID, Objectname, Objectabbr, Startdate, EndDate, IT, LSTUPDT)
VALUES('01', 'O', '50060088', 'Seksi Prwt Chock & Bantalan', '34204', '2010-06-30', '2014-07-01', NULL, '2019-12-31');
INSERT INTO dummy_org_text
(PV, OT, ObjectID, Objectname, Objectabbr, Startdate, EndDate, IT, LSTUPDT)
VALUES('01', 'O', '50060089', 'Urusan Prwt Chock & Bantalan', '34202', '2008-03-13', '2010-06-29', NULL, '2019-12-31');
INSERT INTO dummy_org_text
(PV, OT, ObjectID, Objectname, Objectabbr, Startdate, EndDate, IT, LSTUPDT)
VALUES('01', 'O', '50060089', 'Urusan Prwt Chock & Bantalan', '34204', '2010-06-30', '2014-07-01', NULL, '2019-12-31');
INSERT INTO dummy_org_text
(PV, OT, ObjectID, Objectname, Objectabbr, Startdate, EndDate, IT, LSTUPDT)
VALUES('01', 'O', '50060120', 'Seksi Operasi Penanganan Akhir Material', '34203', '2008-03-13', '2010-06-29', NULL, '2019-12-31');
INSERT INTO dummy_org_text
(PV, OT, ObjectID, Objectname, Objectabbr, Startdate, EndDate, IT, LSTUPDT)
VALUES('01', 'O', '50060120', 'Seksi Operasi Penanganan Akhir Material', '34205', '2010-06-30', '2012-01-30', NULL, '2019-12-31');
INSERT INTO dummy_org_text
(PV, OT, ObjectID, Objectname, Objectabbr, Startdate, EndDate, IT, LSTUPDT)
VALUES('01', 'O', '50060120', 'Seksi Operasi Penanganan Akhir Material', '34204', '2012-01-31', '9999-12-31', NULL, '2019-12-31');
INSERT INTO dummy_org_text
(PV, OT, ObjectID, Objectname, Objectabbr, Startdate, EndDate, IT, LSTUPDT)
VALUES('01', 'O', '50060125', 'Urusan Operasi Shearing Line I', '34203', '2008-03-13', '2010-06-29', NULL, '2019-12-31');
INSERT INTO dummy_org_text
(PV, OT, ObjectID, Objectname, Objectabbr, Startdate, EndDate, IT, LSTUPDT)
VALUES('01', 'O', '50060125', 'Urusan Operasi Shearing Line I', '34205', '2010-06-30', '2014-04-14', NULL, '2019-12-31');
INSERT INTO dummy_org_text
(PV, OT, ObjectID, Objectname, Objectabbr, Startdate, EndDate, IT, LSTUPDT)
VALUES('01', 'O', '50060125', 'Urusan Operasi Shearing Line I', '34204', '2014-04-15', '9999-12-31', NULL, '2019-12-31');
INSERT INTO dummy_org_text
(PV, OT, ObjectID, Objectname, Objectabbr, Startdate, EndDate, IT, LSTUPDT)
VALUES('01', 'O', '50060126', 'Urusan Operasi Shearing Line II', '34203', '2008-03-13', '2010-06-29', NULL, '2019-12-31');
INSERT INTO dummy_org_text
(PV, OT, ObjectID, Objectname, Objectabbr, Startdate, EndDate, IT, LSTUPDT)
VALUES('01', 'O', '50060126', 'Urusan Operasi Shearing Line II', '34204', '2010-06-30', '9999-12-31', NULL, '2019-12-31');
INSERT INTO dummy_org_text
(PV, OT, ObjectID, Objectname, Objectabbr, Startdate, EndDate, IT, LSTUPDT)
VALUES('01', 'O', '50062733', 'Seksi Penanganan HR Coil WIP', '34205', '2012-01-31', '2014-04-14', NULL, '2019-12-31');
INSERT INTO dummy_org_text
(PV, OT, ObjectID, Objectname, Objectabbr, Startdate, EndDate, IT, LSTUPDT)
VALUES('01', 'O', '50062733', 'Seksi Penanganan HR Coil WIP', '34204', '2014-04-15', '2014-06-26', NULL, '2019-12-31');
INSERT INTO dummy_org_text
(PV, OT, ObjectID, Objectname, Objectabbr, Startdate, EndDate, IT, LSTUPDT)
VALUES('01', 'O', '50062734', 'Seksi Penanganan HR Plate WIP', '34205', '2012-01-31', '2014-07-01', NULL, '2019-12-31');
INSERT INTO dummy_org_text
(PV, OT, ObjectID, Objectname, Objectabbr, Startdate, EndDate, IT, LSTUPDT)
VALUES('01', 'O', '50062736', 'Urusan Penang Hasil Prod Coil - WIP', '34205', '2012-01-31', '2014-04-14', NULL, '2019-12-31');
INSERT INTO dummy_org_text
(PV, OT, ObjectID, Objectname, Objectabbr, Startdate, EndDate, IT, LSTUPDT)
VALUES('01', 'O', '50062736', 'Urusan Penang Hasil Prod Coil - WIP', '34204', '2014-04-15', '2014-06-26', NULL, '2019-12-31');
INSERT INTO dummy_org_text
(PV, OT, ObjectID, Objectname, Objectabbr, Startdate, EndDate, IT, LSTUPDT)
VALUES('01', 'O', '50062763', 'Urusan Penanganan Hasil Prod  Plate WIP', '34205', '2012-01-31', '2014-07-01', NULL, '2019-12-31');
INSERT INTO dummy_org_text
(PV, OT, ObjectID, Objectname, Objectabbr, Startdate, EndDate, IT, LSTUPDT)
VALUES('01', 'O', '50078104', 'Urusan Operasi HSPM', '34203', '2008-03-13', '2010-06-29', NULL, '2019-12-31');
INSERT INTO dummy_org_text
(PV, OT, ObjectID, Objectname, Objectabbr, Startdate, EndDate, IT, LSTUPDT)
VALUES('01', 'O', '50078104', 'Urusan Operasi HSPM', '34204', '2010-06-30', '9999-12-31', NULL, '2019-12-31');
INSERT INTO dummy_org_text
(PV, OT, ObjectID, Objectname, Objectabbr, Startdate, EndDate, IT, LSTUPDT)
VALUES('01', 'O', '50078105', 'Urusan Bengkel SL & HSPM', '34203', '2008-03-13', '2010-06-29', NULL, '2019-12-31');
INSERT INTO dummy_org_text
(PV, OT, ObjectID, Objectname, Objectabbr, Startdate, EndDate, IT, LSTUPDT)
VALUES('01', 'O', '50078105', 'Urusan Bengkel SL & HSPM', '34205', '2010-06-30', '2014-07-01', NULL, '2019-12-31');
INSERT INTO dummy_org_text
(PV, OT, ObjectID, Objectname, Objectabbr, Startdate, EndDate, IT, LSTUPDT)
VALUES('01', 'O', '50085540', 'Dinas Preparasi Roll & Bearing PBLP', '34204', '2010-06-30', '2011-12-08', NULL, '2019-12-31');
INSERT INTO dummy_org_text
(PV, OT, ObjectID, Objectname, Objectabbr, Startdate, EndDate, IT, LSTUPDT)
VALUES('01', 'O', '50085540', 'Dinas Preparasi Roll & bearing PBLP', '34204', '2011-12-09', '2012-01-30', NULL, '2019-12-31');
INSERT INTO dummy_org_text
(PV, OT, ObjectID, Objectname, Objectabbr, Startdate, EndDate, IT, LSTUPDT)
VALUES('01', 'O', '50085540', 'Dinas Roll & Bearing Preparation HSM', '34203', '2012-01-31', '9999-12-31', NULL, '2019-12-31');
INSERT INTO dummy_org_text
(PV, OT, ObjectID, Objectname, Objectabbr, Startdate, EndDate, IT, LSTUPDT)
VALUES('01', 'O', '50085542', 'Seksi Penanganan Roll Mill', '34204', '2010-06-30', '2014-07-01', NULL, '2019-12-31');
INSERT INTO dummy_org_text
(PV, OT, ObjectID, Objectname, Objectabbr, Startdate, EndDate, IT, LSTUPDT)
VALUES('01', 'O', '50085543', 'Seksi Penanganan Roll SL & HSPM', '34204', '2010-06-30', '2014-07-01', NULL, '2019-12-31');
INSERT INTO dummy_org_text
(PV, OT, ObjectID, Objectname, Objectabbr, Startdate, EndDate, IT, LSTUPDT)
VALUES('01', 'O', '50085544', 'Seksi Pengdl Roll Mill,SL & HSPM', '34204', '2010-06-30', '2014-07-01', NULL, '2019-12-31');
INSERT INTO dummy_org_text
(PV, OT, ObjectID, Objectname, Objectabbr, Startdate, EndDate, IT, LSTUPDT)
VALUES('01', 'O', '50104622', 'Seksi Reheating Furnace', '34202', '2014-04-15', '9999-12-31', NULL, '2019-12-31');
INSERT INTO dummy_org_text
(PV, OT, ObjectID, Objectname, Objectabbr, Startdate, EndDate, IT, LSTUPDT)
VALUES('01', 'O', '50104623', 'Seksi Mill Operation HSM', '34202', '2014-04-15', '9999-12-31', NULL, '2019-12-31');
INSERT INTO dummy_org_text
(PV, OT, ObjectID, Objectname, Objectabbr, Startdate, EndDate, IT, LSTUPDT)
VALUES('01', 'O', '50104624', 'Seksi Parameter Process Control HSM', '34202', '2014-04-15', '9999-12-31', NULL, '2019-12-31');
INSERT INTO dummy_org_text
(PV, OT, ObjectID, Objectname, Objectabbr, Startdate, EndDate, IT, LSTUPDT)
VALUES('01', 'O', '50104625', 'Urusan Reheating Furnace', '34202', '2014-04-15', '9999-12-31', NULL, '2019-12-31');
INSERT INTO dummy_org_text
(PV, OT, ObjectID, Objectname, Objectabbr, Startdate, EndDate, IT, LSTUPDT)
VALUES('01', 'O', '50104650', 'Urusan Roughing Mill', '34202', '2014-04-15', '9999-12-31', NULL, '2019-12-31');
INSERT INTO dummy_org_text
(PV, OT, ObjectID, Objectname, Objectabbr, Startdate, EndDate, IT, LSTUPDT)
VALUES('01', 'O', '50104726', 'Urusan Finishing Mill', '34202', '2014-04-15', '9999-12-31', NULL, '2019-12-31');
INSERT INTO dummy_org_text
(PV, OT, ObjectID, Objectname, Objectabbr, Startdate, EndDate, IT, LSTUPDT)
VALUES('01', 'O', '50104727', 'Urusan Down Coiler', '34202', '2014-04-15', '9999-12-31', NULL, '2019-12-31');
INSERT INTO dummy_org_text
(PV, OT, ObjectID, Objectname, Objectabbr, Startdate, EndDate, IT, LSTUPDT)
VALUES('01', 'O', '50104956', 'Seksi Roller Table Mill, SL & HSPM', '34203', '2014-04-15', '9999-12-31', NULL, '2019-12-31');
INSERT INTO dummy_org_text
(PV, OT, ObjectID, Objectname, Objectabbr, Startdate, EndDate, IT, LSTUPDT)
VALUES('01', 'O', '50104957', 'Seksi Roll Grinding & Roll Quality', '34203', '2014-04-15', '9999-12-31', NULL, '2019-12-31');
INSERT INTO dummy_org_text
(PV, OT, ObjectID, Objectname, Objectabbr, Startdate, EndDate, IT, LSTUPDT)
VALUES('01', 'O', '50104958', 'Seksi Chock & Bearing', '34203', '2014-04-15', '9999-12-31', NULL, '2019-12-31');
INSERT INTO dummy_org_text
(PV, OT, ObjectID, Objectname, Objectabbr, Startdate, EndDate, IT, LSTUPDT)
VALUES('01', 'O', '50104968', 'Urusan Roll Grinding & Roll Quality', '34203', '2014-04-15', '9999-12-31', NULL, '2019-12-31');
INSERT INTO dummy_org_text
(PV, OT, ObjectID, Objectname, Objectabbr, Startdate, EndDate, IT, LSTUPDT)
VALUES('01', 'O', '50104969', 'Urusan Maintenance Grinding Machine', '34203', '2014-04-15', '9999-12-31', NULL, '2019-12-31');
INSERT INTO dummy_org_text
(PV, OT, ObjectID, Objectname, Objectabbr, Startdate, EndDate, IT, LSTUPDT)
VALUES('01', 'O', '50105055', 'Urusan Chock & Bearing', '34203', '2014-04-15', '9999-12-31', NULL, '2019-12-31');
INSERT INTO dummy_org_text
(PV, OT, ObjectID, Objectname, Objectabbr, Startdate, EndDate, IT, LSTUPDT)
VALUES('01', 'O', '50105067', 'Dinas Inspection & TS Mechanical HSM', '34205', '2014-04-15', '2017-08-01', NULL, '2019-12-31');
INSERT INTO dummy_org_text
(PV, OT, ObjectID, Objectname, Objectabbr, Startdate, EndDate, IT, LSTUPDT)
VALUES('01', 'O', '50105129', 'Urusan Fluid System', '34205', '2014-04-15', '2017-05-07', NULL, '2019-12-31');
INSERT INTO dummy_org_text
(PV, OT, ObjectID, Objectname, Objectabbr, Startdate, EndDate, IT, LSTUPDT)
VALUES('01', 'O', '50105168', 'Seksi Mechanical Mill Mechanis', '34205', '2014-04-15', '2017-08-01', NULL, '2019-12-31');
INSERT INTO dummy_org_text
(PV, OT, ObjectID, Objectname, Objectabbr, Startdate, EndDate, IT, LSTUPDT)
VALUES('01', 'O', '50105170', 'Urusan Mechanical Mill Mechanis', '34205', '2014-04-15', '2017-08-01', NULL, '2019-12-31');
INSERT INTO dummy_org_text
(PV, OT, ObjectID, Objectname, Objectabbr, Startdate, EndDate, IT, LSTUPDT)
VALUES('01', 'O', '50105188', 'Seksi Mechanical Mill-HLP', '34205', '2014-04-15', '2017-08-01', NULL, '2019-12-31');
INSERT INTO dummy_org_text
(PV, OT, ObjectID, Objectname, Objectabbr, Startdate, EndDate, IT, LSTUPDT)
VALUES('01', 'O', '50105194', 'Urusan Mechanical Mill-HLP', '34205', '2014-04-15', '2017-08-01', NULL, '2019-12-31');
INSERT INTO dummy_org_text
(PV, OT, ObjectID, Objectname, Objectabbr, Startdate, EndDate, IT, LSTUPDT)
VALUES('01', 'O', '50105235', 'Seksi Mechanical SL & HSPM', '34205', '2014-04-15', '2017-08-01', NULL, '2019-12-31');
INSERT INTO dummy_org_text
(PV, OT, ObjectID, Objectname, Objectabbr, Startdate, EndDate, IT, LSTUPDT)
VALUES('01', 'O', '50105243', 'Urusan Mechanical SL & HSPM', '34205', '2014-04-15', '2017-08-01', NULL, '2019-12-31');
INSERT INTO dummy_org_text
(PV, OT, ObjectID, Objectname, Objectabbr, Startdate, EndDate, IT, LSTUPDT)
VALUES('01', 'O', '50105259', 'Seksi Mechanical Auxiliary', '34205', '2014-04-15', '2017-08-01', NULL, '2019-12-31');
INSERT INTO dummy_org_text
(PV, OT, ObjectID, Objectname, Objectabbr, Startdate, EndDate, IT, LSTUPDT)
VALUES('01', 'O', '50105261', 'Urusan Mechanical Auxiliary', '34205', '2014-04-15', '2017-08-01', NULL, '2019-12-31');
INSERT INTO dummy_org_text
(PV, OT, ObjectID, Objectname, Objectabbr, Startdate, EndDate, IT, LSTUPDT)
VALUES('01', 'O', '50105287', 'Urusan Workshop SL & HSPM', '34204', '2014-04-15', '9999-12-31', NULL, '2019-12-31');
INSERT INTO dummy_org_text
(PV, OT, ObjectID, Objectname, Objectabbr, Startdate, EndDate, IT, LSTUPDT)
VALUES('01', 'O', '50105355', 'Seksi Handling HR Coil WIP', '34204', '2014-04-15', '9999-12-31', NULL, '2019-12-31');
INSERT INTO dummy_org_text
(PV, OT, ObjectID, Objectname, Objectabbr, Startdate, EndDate, IT, LSTUPDT)
VALUES('01', 'O', '50105356', 'Urusan Handling Finished Prod Coil-WIP', '34204', '2014-04-15', '9999-12-31', NULL, '2019-12-31');
INSERT INTO dummy_org_text
(PV, OT, ObjectID, Objectname, Objectabbr, Startdate, EndDate, IT, LSTUPDT)
VALUES('01', 'O', '50105360', 'Seksi Handling HR Plate WIP', '34204', '2014-04-15', '9999-12-31', NULL, '2019-12-31');
INSERT INTO dummy_org_text
(PV, OT, ObjectID, Objectname, Objectabbr, Startdate, EndDate, IT, LSTUPDT)
VALUES('01', 'O', '50105361', 'Urusan Handling HR Plate WIP', '34204', '2014-04-15', '9999-12-31', NULL, '2019-12-31');
INSERT INTO dummy_org_text
(PV, OT, ObjectID, Objectname, Objectabbr, Startdate, EndDate, IT, LSTUPDT)
VALUES('01', 'O', '50105404', 'Dinas Inspection & Troubleshoot AEI HSM', '34206', '2014-04-15', '2017-08-01', NULL, '2019-12-31');
INSERT INTO dummy_org_text
(PV, OT, ObjectID, Objectname, Objectabbr, Startdate, EndDate, IT, LSTUPDT)
VALUES('01', 'O', '50105405', 'Seksi Level 1-2 Automation HSM', '34206', '2014-04-15', '2017-08-01', NULL, '2019-12-31');
INSERT INTO dummy_org_text
(PV, OT, ObjectID, Objectname, Objectabbr, Startdate, EndDate, IT, LSTUPDT)
VALUES('01', 'O', '50105406', 'Seksi Level 0 Electric & Instr HSM', '34206', '2014-04-15', '2017-08-01', NULL, '2019-12-31');
INSERT INTO dummy_org_text
(PV, OT, ObjectID, Objectname, Objectabbr, Startdate, EndDate, IT, LSTUPDT)
VALUES('01', 'O', '50105407', 'Seksi Electrical SL & HSPM', '34206', '2014-04-15', '2017-08-01', NULL, '2019-12-31');
INSERT INTO dummy_org_text
(PV, OT, ObjectID, Objectname, Objectabbr, Startdate, EndDate, IT, LSTUPDT)
VALUES('01', 'O', '50105408', 'Seksi Electrical Auxiliary', '34206', '2014-04-15', '2017-08-01', NULL, '2019-12-31');
INSERT INTO dummy_org_text
(PV, OT, ObjectID, Objectname, Objectabbr, Startdate, EndDate, IT, LSTUPDT)
VALUES('01', 'O', '50105409', 'Urusan Level 0 Electric & Instrumen HSM', '34206', '2014-04-15', '2017-08-01', NULL, '2019-12-31');
INSERT INTO dummy_org_text
(PV, OT, ObjectID, Objectname, Objectabbr, Startdate, EndDate, IT, LSTUPDT)
VALUES('01', 'O', '50105410', 'Urusan Level 0 SL & HSPM', '34206', '2014-04-15', '2017-08-01', NULL, '2019-12-31');
INSERT INTO dummy_org_text
(PV, OT, ObjectID, Objectname, Objectabbr, Startdate, EndDate, IT, LSTUPDT)
VALUES('01', 'O', '50105411', 'Urusan Electrical Auxiliary', '34206', '2014-04-15', '2017-08-01', NULL, '2019-12-31');
INSERT INTO dummy_org_text
(PV, OT, ObjectID, Objectname, Objectabbr, Startdate, EndDate, IT, LSTUPDT)
VALUES('01', 'O', '50105445', 'Urusan Level 1-2 Automation HSM', '34206', '2014-04-15', '2017-08-01', NULL, '2019-12-31');
INSERT INTO dummy_org_text
(PV, OT, ObjectID, Objectname, Objectabbr, Startdate, EndDate, IT, LSTUPDT)
VALUES('01', 'O', '50106213', 'Staf Temporary', '34200', '2014-04-15', '2018-10-01', NULL, '2019-12-31');
INSERT INTO dummy_org_text
(PV, OT, ObjectID, Objectname, Objectabbr, Startdate, EndDate, IT, LSTUPDT)
VALUES('01', 'O', '50106349', 'Urusan Level 1 SL & HSPM', '34206', '2014-04-15', '2017-08-01', NULL, '2019-12-31');
INSERT INTO dummy_org_text
(PV, OT, ObjectID, Objectname, Objectabbr, Startdate, EndDate, IT, LSTUPDT)
VALUES('01', 'O', '50121008', 'Staf BKO HSM', '34205', '2018-08-01', '2019-11-30', NULL, '2019-12-31');
INSERT INTO dummy_org_text
(PV, OT, ObjectID, Objectname, Objectabbr, Startdate, EndDate, IT, LSTUPDT)
VALUES('01', 'O', '50121008', 'Staf BKO HSM', '34200', '2019-12-01', '9999-12-31', NULL, '2019-12-31');
INSERT INTO dummy_org_text 
(PV, OT, ObjectID, Objectname, Objectabbr, Startdate, EndDate, IT, LSTUPDT) 
VALUES('01', 'O', '50002929', 'Subdit Rolling Mill', '34000', '2011-10-07', '9999-12-31', NULL, '2019-11-22');
