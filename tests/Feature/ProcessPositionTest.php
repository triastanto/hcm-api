<?php

namespace Tests\Feature;

use App\Models\HCI\SPosition;
use App\PositionProcessor;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ProcessPositionTest extends TestCase
{
    public function testProcessPosition()
    {
        SPosition::get()->each(function ($item) {
            $processor = new PositionProcessor($item);
            $processor->process();
        });
    }
}
