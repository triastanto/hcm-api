<?php

namespace Tests\Feature;

use App\Models\HCI\SvoHead;
use App\OrgPositionHeadProcessor;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ProcessOrgPosHeadTest extends TestCase
{
    public function testProcessOrgPosHead()
    {
        SvoHead::get()->each(function ($item) {
            $processor = new OrgPositionHeadProcessor($item);
            $processor->process();
        });
    }
}
