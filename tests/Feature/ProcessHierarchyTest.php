<?php

namespace Tests\Feature;

use App\Models\HCI\Orgunit;
use App\OrgHierarchyProcessor;
use Tests\TestCase;

class ProcessHierarchyTest extends TestCase
{
    public function testProcessHierarcyObjectOrientedStyle()
    {
        Orgunit::get()->each(function ($item) { 
            $processor = new OrgHierarchyProcessor($item);
            $processor->traceOrgHistory($processor->parent, $processor->getParentPayload());
            $processor->traceOrgHistory($processor->child, $processor->getChildPayload());
            $processor->addRelationship();
        });
    }
}
