<?php

use App\Http\Controllers\API\Search\SearchController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// later need to add 'middleware' => 'auth:api'
Route::group([ 'namespace' => 'API', 'as' => 'api.'], function () {
    Route::group(['namespace' => 'Search', 'prefix' => 'search', 'as' => 'search.'], function () {
        Route::group(['prefix' => 'organizations'], function () {
            Route::get('/', [SearchController::class, 'organizations'])->name('organizations');
            Route::group(['prefix' => '{organization}', 'as' => 'organizations.'], function () {
                Route::get('', [SearchController::class, 'organizationById'])->name('show');
                Route::get('parent', [SearchController::class, 'organizationParent'])->name('parent');
                Route::get('children', [SearchController::class, 'organizationChildren'])->name('children');
                Route::get('history', [SearchController::class, 'organizationHistory'])->name('history');
                Route::get('positions', [SearchController::class, 'organizationPosition'])->name('position');
                Route::get('positions/head', [SearchController::class, 'organizationPositionHead'])->name('position.head');
            });
        });
        Route::group(['prefix' => 'employees'], function () {
            Route::get('/', [SearchController::class, 'employees'])->name('employees');
            Route::get('{employee}', [SearchController::class, 'employeeByPersonnelno'])->name('employees.show');
        });
    });
});