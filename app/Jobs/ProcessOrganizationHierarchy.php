<?php

namespace App\Jobs;

use App\Models\HCI\Orgunit;
use App\OrgHierarchyProcessor;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class ProcessOrganizationHierarchy implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        Orgunit::get()->each(function ($item) { 
            $processor = new OrgHierarchyProcessor($item);
            $processor->traceOrgHistory($processor->parent, $processor->getParentPayload());
            $processor->traceOrgHistory($processor->child, $processor->getChildPayload());
            $processor->addRelationship();
        });
    }
}
