<?php

namespace App;

use App\Models\HCI\StructdispSap;
use App\Models\Organization;
use App\Models\Position;

class OrgPosProcessor
{
    protected $structdispsap;

    public function __construct(StructdispSap $structdispsap)
    {
        $this->structdispsap = $structdispsap;
    }

    protected function getSapOrgObjId()
    {
        return $this->structdispsap->emporid;
    }

    protected function getSapPosObjId()
    {
        return $this->structdispsap->empposid;
    }

    protected function getOrganization()
    {
        return Organization::whereObjectId($this->getSapOrgObjId())->first();
    }

    protected function getPosition()
    {
        return Position::whereObjectId($this->getSapPosObjId())->first();
    }

    public function add()
    {
        $organization = $this->getOrganization();
        $position = $this->getPosition();
        
        if ($organization && $position) {
            $position->organization_id = $organization->id;
            $position->save();
            
            return $position;
        }

        return false;
    }
}
