<?php

namespace App;

use App\Models\Employee;
use App\Models\HCI\StructdispSap;
use App\Models\Position;

class EmployeeProcessor
{
    protected $structdispsap;

    public function __construct(StructdispSap $structdispsap)
    {
        $this->structdispsap = $structdispsap;
    }

    protected function getSapOrgObjId()
    {
        return $this->structdispsap->emporid;
    }

    protected function getSapPosObjId()
    {
        return $this->structdispsap->empposid;
    }

    protected function getPersonnelNo()
    {
        return $this->structdispsap->empnik;
    }

    public function add()
    {
        $position = Position::whereObjectId($this->getSapPosObjId())->first();

        if ($position) {
            return Employee::updateOrCreate([
                'personnel_no' => $this->getPersonnelNo(),
                'position_id' => $position->id
            ]);
        }

        return false;
    }
}
