<?php
namespace App\Models;

use Franzose\ClosureTable\Models\ClosureTable;

class OrghistoryClosure extends ClosureTable implements OrghistoryClosureInterface
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'orghistory_closure';
}
