<?php
namespace App\Models;

use Franzose\ClosureTable\Models\ClosureTable;

class OrganizationClosure extends ClosureTable implements OrganizationClosureInterface
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'organization_closure';
}
