<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Position extends Model
{
    use ObjectPayloadScope;
    
    protected $fillable = [
        'sap_object_id',
        'sap_object_abbr',
        'name',
    ];

    public function organization()
    {
        return $this->belongsTo(Organization::class);
    }

    public function setAsHead() {
        $this->is_head = true;
        $this->save();
    }
}
