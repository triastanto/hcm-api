<?php

namespace App\Models;

use Franzose\ClosureTable\Models\Entity;

class Organization extends Entity implements OrganizationInterface
{
    use ObjectPayloadScope;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'organizations';

    /**
     * ClosureTable model instance.
     *
     * @var OrganizationClosure
     */
    protected $closure = 'App\Models\OrganizationClosure';

    /**
     * Gets the short name of the "position" column.
     *
     * @return string
     */
    public function getPositionColumn()
    {
        return 'closure_position';
    }

    /**
     * Indicates if the model should soft delete.
     *
     * @var bool
     */
    protected $softDelete = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'sap_object_id',
        'sap_object_abbr'
    ];

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

    public function orghistories()
    {
        return $this->hasMany(Orghistory::class);
    }

    public function positions()
    {
        return $this->hasMany(Position::class);
    }

    public function positionHead()
    {
        return $this->positions()->where('is_head', 1);
    }

    public function getOrghistoryRoot()
    {
        $orghistories = $this->orghistories;

        return ($orghistories) ? $orghistories->where('parent_id', null)->first() : null;
    }

    public function getPositionHead()
    {
        $head = $this->positionHead;

        return ($head) ? $head->first() : null;
    }
}
