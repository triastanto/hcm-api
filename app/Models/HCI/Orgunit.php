<?php

namespace App\Models\HCI;

use App\Models\HCI\Scopes\ActiveScope;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\App;

class Orgunit extends Model
{
    /**
     * primaryKey 
     * 
     * @var integer
     * @access protected
     */
    protected $primaryKey = null;

    /**
     * Indicates if the IDs are auto-incrementing.
     *
     * @var bool
     */
    public $incrementing = false;

    /**
     * Create a new Eloquent model instance.
     *
     * @param  array  $attributes
     * @return void
     */
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);

        if (App::environment('production')) {
            $this->setConnection('hci_mysql');
        }

        $this->setTable(config('hcm.table.orgunit'));
    }

    /**
     * The "booting" method of the model.
     *
     * @return void
     */
    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope(new ActiveScope);
    }

    public function parentOrgtext()
    {
        return $this->hasOne(Orgtext::class, 'ObjectID', 'IDrelatedobject');
    }

    public function childOrgtext()
    {
        return $this->hasOne(Orgtext::class, 'ObjectID', 'ObjectID');
    }
}
