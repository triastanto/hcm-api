<?php
namespace App\Models;

use Franzose\ClosureTable\Contracts\ClosureTableInterface;

interface OrganizationClosureInterface extends ClosureTableInterface
{
}
