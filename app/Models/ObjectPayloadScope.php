<?php

namespace App\Models;

trait ObjectPayloadScope
{
    /**
     * Query scope to search by composite key
     * 
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @param  string $objectId
     * @param  string $objectAbbr
     * @param  string $name
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeWhereObjIdObjAbbrName($query, $objectId, $objectAbbr, $name)
    {
        return $query->whereObjectId((int) $objectId)
            ->where('sap_object_abbr', (int) $objectAbbr)
            ->where('name', (string) $name);
    }

    /**
     * Query scope to search by sap object id key
     * 
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @param  string $objectId
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeWhereObjectId($query, $objectId)
    {
        return $query->where('sap_object_id', (int) $objectId);
    }
}
