<?php
namespace App\Models;

use Franzose\ClosureTable\Models\Entity;

class Orghistory extends Entity implements OrghistoryInterface
{
    use ObjectPayloadScope;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'orghistories';

    /**
     * ClosureTable model instance.
     *
     * @var OrghistoryClosure
     */
    protected $closure = 'App\Models\OrghistoryClosure';

    /**
     * Gets the short name of the "position" column.
     *
     * @return string
     */
    public function getPositionColumn()
    {
        return 'closure_position';
    }

    /**
     * Indicates if the model should soft delete.
     *
     * @var bool
     */
    protected $softDelete = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'sap_object_id',
        'sap_object_abbr',
        'organization_id'
    ];

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

    public function getDescendantsWithRoot()
    {
        $descendants = $this->getDescendants();

        return $descendants->prepend($this);
    }
}
