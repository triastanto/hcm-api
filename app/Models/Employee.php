<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{
    protected $fillable = [
        'personnel_no',
        'position_id'
    ];

    protected $primaryKey = 'personnel_no';

    public $incrementing = false;

    public function position()
    {
        return $this->belongsTo(Position::class);
    }
}
