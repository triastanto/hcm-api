<?php
namespace App\Models;

use Franzose\ClosureTable\Contracts\ClosureTableInterface;

interface OrghistoryClosureInterface extends ClosureTableInterface
{
}
