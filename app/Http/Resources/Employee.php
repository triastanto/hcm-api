<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Employee extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $employee = parent::toArray($request);

        return [
            'personnel_no' => $this->personnel_no,
            'position' => new Position($this->position),
            'created_at' => $employee['created_at'],
            'updated_at' => $employee['updated_at']
        ];
    }
}
