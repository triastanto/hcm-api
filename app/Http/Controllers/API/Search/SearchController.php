<?php

namespace App\Http\Controllers\API\Search;

use App\Http\Controllers\Controller;
use App\Http\Resources\Employee as ResourcesEmployee;
use App\Models\Employee;
use App\Models\Organization;

class SearchController extends Controller
{
    public function organizations()
    {
        $organizations = Organization::paginate(10);

        return response()->json($organizations);
    }

    public function organizationById(Organization $organization)
    {
        return $organization;
    }

    public function organizationParent(Organization $organization)
    {
        $parent = $organization->getParent();

        return response()->json($parent);
    }

    public function organizationChildren(Organization $organization)
    {
        $children = $organization->getChildren();

        return response()->json($children);
    }

    public function organizationHistory(Organization $organization)
    {
        $orghistoryRoot = $organization->getOrghistoryRoot();
        
        return response()->json($orghistoryRoot->getDescendantsWithRoot());
    }

    public function organizationPosition(Organization $organization)
    {
        return response()->json($organization->positions);
    }

    public function organizationPositionHead(Organization $organization)
    {
        return response()->json($organization->getPositionHead());
    }

    public function employees()
    {
        $employees = Employee::paginate(10);

        return (ResourcesEmployee::collection($employees));
    }

    public function employeeByPersonnelno(Employee $employee)
    {
        return (new ResourcesEmployee($employee));
    }
}
