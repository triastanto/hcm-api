<?php

namespace App;

use App\Models\HCI\SvoHead;
use App\Models\Organization;
use App\Models\Position;

class OrgPositionHeadProcessor
{
    protected $svo;

    public function __construct(SvoHead $svo)
    {
        $this->svo = $svo;
    }

    protected function getSapPosObjId()
    {
        return $this->svo->ObjectID;
    }

    protected function getSapOrgObjId()
    {
        return $this->svo->IDrelatedobject;
    }

    protected function getOrganization()
    {
        return Organization::whereObjectId($this->getSapOrgObjId())->first();
    }

    protected function getPosition()
    {
        return Position::whereObjectId($this->getSapPosObjId())->first();
    }

    public function process()
    {
        $organization = $this->getOrganization();
        $position = $this->getPosition();

        if ($organization && $position) {
            if ($position->organization_id == $organization->id) {
                $position->setAsHead();

                return $position;
            }
        }

        return false;
    }
}