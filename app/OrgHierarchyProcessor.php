<?php

namespace App;

use App\Models\HCI\Orgunit;
use App\Models\Organization;
use App\Models\Orghistory;

class OrgHierarchyProcessor
{
    protected $orgunit;
    public $parent;
    public $child;
    public $parentHistory;
    public $childHistory;

    public function __construct(Orgunit $orgunit)
    {
        $this->orgunit = $orgunit;
        $this->parent = $this->updateOrCreateOrg($this->getParentSOI(), $this->getParentPayload());
        $this->child = $this->updateOrCreateOrg($this->getChildSOI(), $this->getChildPayload());
    }

    public function addRelationship()
    {
        $this->parent->addChild($this->child);
    }

    public function traceOrgHistory(Organization $org, $payload)
    {
        // cari record berdasarkan sap_object_id di model Orghistory
        $sameObjectId = Orghistory::whereObjectId($org->sap_object_id)->first();

        // cari descendant terkahir (depth & position paling tinggi), jika tidak gunakan sameObjectId
        if ($sameObjectId) {
            if ($sameObjectId->getLastChild()) {
                $maxDepth = $sameObjectId->max('closure_real_depth');
                $maxDepthChildren = $sameObjectId->getDescendantsWhere('closure_real_depth', '=', $maxDepth);
                $maxPosition = $maxDepthChildren->max('closure_position');
                $lastDescendant = $maxDepthChildren->where('closure_position', $maxPosition)->first();
            } else
                $lastDescendant = $sameObjectId;
        }

        // cari record berdasarkan parentData
        $orgHistory = forward_static_call_array(
            [Orghistory::class, 'whereObjIdObjAbbrName'],
            $payload
        )->first();

        // jika tidak ditemukan maka buat record baru Orghistory
        if (is_null($orgHistory)) {
            $payload += [ 'organization_id' => $org->id ];
            $orgHistory = Orghistory::create($payload);
        }
        // jika parentHistory adalah hasil record baru dan memiliki sameObjectId
        if ($orgHistory->wasRecentlyCreated && $sameObjectId) {

            // tambahkan sebagai child dari lastDescendant
            $lastDescendant->addChild($orgHistory);
        }
    }

    protected function updateOrCreateOrg($soi, $payload)
    {
        $org = Organization::whereObjectId($soi)->first();
        if (!is_null($org)) {
            $org->update($payload);
            $org->refresh();
        } else
            $org = Organization::create($payload);

        return $org;
    }

    public function getParentPayload()
    {
        return [
            'sap_object_id' => $this->getParentSOI(),
            'sap_object_abbr' => $this->getParentSOA(),
            'name' => $this->getParentName(),
        ];
    }

    public function getChildPayload()
    {
        return [
            'sap_object_id' => $this->getChildSOI(),
            'sap_object_abbr' => $this->getChildSOA(),
            'name' => $this->getChildName(),
        ];
    }

    public function getParentSOI()
    {
        return (int) $this->orgunit->IDrelatedobject;
    }

    public function getParentSOA()
    {
        return (int) $this->orgunit->ObjAbbr;
    }

    public function getParentName()
    {
        $parentOrgtext = $this->orgunit->parentOrgtext;

        return ($parentOrgtext) ? $parentOrgtext->Objectname : null;
    }

    public function getChildSOI()
    {
        return (int) $this->orgunit->ObjectID;
    }

    public function getChildSOA()
    {
        return (int) $this->orgunit->IDObjAbbr;
    }

    public function getChildName()
    {
        $childOrgtext = $this->orgunit->childOrgtext;

        return ($childOrgtext) ? $childOrgtext->Objectname : null;
    }
}
