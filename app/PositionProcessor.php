<?php

namespace App;

use App\Models\HCI\SPosition;
use App\Models\Position;

class PositionProcessor
{
    protected $sposition;

    public function __construct(SPosition $sposition)
    {
        $this->sposition = $sposition;
    }

    protected function getSapObjId()
    {
        return $this->sposition->ObjectID;
    }

    protected function getSapObjAbbr()
    {
        return $this->sposition->Objectabbr;
    }

    protected function getSapObjectName()
    {
        return $this->sposition->Objectname;
    }

    protected function getPayload()
    {
        return [
            'sap_object_id' => $this->getSapObjId(),
            'sap_object_abbr' => $this->getSapObjAbbr(),
            'name' => $this->getSapObjectName()
        ];
    }

    protected function updateOrCreatePosition($soi, $payload)
    {
        $position = Position::whereObjectId($soi)->first();
        if (!is_null($position)) {
            $position->update($payload);
            $position->refresh();
        } else
            $position = Position::create($payload);
    }

    public function process()
    {
        return $this->updateOrCreatePosition($this->getSapObjId(), $this->getPayload());
    }
}