<?php

return [

    'table' => [
        'orgunit' => env('TABLE_ORGUNIT', 'dummy_orgunit'),
        'org_text' => env('TABLE_ORG_TEXT', 'dummy_org_text'),
        'structdisp_sap' => env('TABLE_STRUCTDISP_SAP', 'dummy_structdisp_sap'),
        'svo_head' => env('TABLE_SVO_HEAD', 'dummy_svo_head'),
        's_positions' => env('TABLE_S_POSITIONS', 'dummy_s_positions')
    ]

];